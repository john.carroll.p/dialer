// import { v4 as uuid } from 'uuid';
import { ICaller } from './models/agent';
import { IList } from './models/list';
import { IOrganization } from './models/organization';

export const organizations_seed: IOrganization[] = [
  {
    id: 'org-1',
    name: 'Showing Up for Racial Justice',
    shortName: 'SURJ',
  },
];

export const lists_seed: IList[] = [
  {
    id: 'list-1',
    organizationId: 'org-1',
    name: 'One',
  },
];

export const callers_seed: ICaller[] = [
  {
    id: 'caller-1',
    organizationId: 'org-1',
    firstName: 'John',
    lastName: 'Carroll',
    emailAddress: 'john@example.org',
    phoneNumber: '831-234-5678',
    online: false,
    status: 'NOT_READY',
  },
];
