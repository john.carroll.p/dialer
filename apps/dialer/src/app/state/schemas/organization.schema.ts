import { RxJsonSchema } from 'rxdb/plugins/core';
import { IOrganization } from '../models/organization';

export const organizationSchema: RxJsonSchema<IOrganization> = {
  title: 'organization schema',
  description: 'describes an organization',
  version: 0,
  keyCompression: false,
  type: 'object',
  properties: {
    id: {
      type: 'string',
      primary: true,
      final: true,
    },
    name: {
      type: 'string',
    },
    shortName: {
      type: 'string',
    },
  },
  required: ['id', 'name', 'shortName'],
};
