// import { RxCollection, RxDocument } from 'rxdb/plugins/core';

export interface IList {
  id: string;
  organizationId: string;
  name: string;
}

// export type RxListDocument = RxDocument<IList>;
// export type RxListCollection = RxCollection<IList>;

// export class List {
//   id: string;
//   organizationId: string;
//   name: string;
//   contacts: Contact[] = [];

//   constructor(args: IList, contacts: IContact[]) {
//     Object.assign(this, args);

//     this.contacts = contacts.map((c) => new Contact(c));
//   }
// }
