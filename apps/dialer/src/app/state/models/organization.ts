// import { RxCollection, RxDocument } from 'rxdb/plugins/core';

export interface IOrganization {
  id: string;
  name: string;
  shortName: string;
}

// export type RxOrganizationDocument = RxDocument<IOrganization>;
// export type RxOrganizationCollection = RxCollection<IOrganization>;

// export class Organization {
//   id: string;
//   name: string;
//   shortName: string;

//   constructor(args: IOrganization) {
//     Object.assign(this, args);
//   }
// }
