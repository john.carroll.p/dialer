export interface IAdmin {
  id: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  emailAddress: string;
  online: boolean;
}

export class Admin {
  id: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  emailAddress: string;
  online: boolean;

  constructor(args: IAdmin) {
    Object.assign(this, args);
  }

  name() {
    return `${this.firstName} ${this.lastName}`;
  }
}
