import { Component, OnInit } from '@angular/core';
import { DatabaseService } from '../../state/database.service';

@Component({
  selector: 'app-callers',
  templateUrl: './callers.component.html',
  styleUrls: ['./callers.component.scss'],
})
export class CallersComponent implements OnInit {
  callers = this.databaseService.getCallers({ organizationId: 'org-1' });

  constructor(private databaseService: DatabaseService) {}

  ngOnInit(): void {}
}
