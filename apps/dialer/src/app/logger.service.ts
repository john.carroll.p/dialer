import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class Logger {
  debug(...data: unknown[]) {
    console.debug(...data);
  }

  info(...data: unknown[]) {
    console.log(...data);
  }

  warn(...data: unknown[]) {
    console.warn(...data);
  }

  error(...data: unknown[]) {
    console.error(...data);
  }
}
