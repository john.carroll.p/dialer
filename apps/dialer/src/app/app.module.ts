import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { IsLoadingPipeModule } from '@service-work/is-loading';
import { MatProgressBarModule } from '@angular/material/progress-bar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { PortalModule } from '@angular/cdk/portal';
// import { initRxDatabase } from './state/database.service';
// import { Logger } from './logger.service';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    IsLoadingPipeModule,
    PortalModule,
    MatProgressBarModule,
    MatToolbarModule,
    MatSidenavModule,
  ],
  providers: [
    // WIP
    // {
    //   provide: APP_INITIALIZER,
    //   useFactory: initRxDatabase,
    //   multi: true,
    //   deps: [Logger],
    // },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
