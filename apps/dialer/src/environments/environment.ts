// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyB5uTLvhUHXX2MHPMyy4CHy_fRzuRhYnxE',
    authDomain: 'dialer-dev.firebaseapp.com',
    databaseURL: 'https://dialer-dev.firebaseio.com',
    projectId: 'dialer-dev',
    storageBucket: 'dialer-dev.appspot.com',
    messagingSenderId: '994750922723',
    appId: '1:994750922723:web:cbbcde02dcd03e8a13ead6',
    measurementId: 'G-EX7Q8YHNBN',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
