import { ValidatorFn } from '@angular/forms';
import { Decoder, isDecoderSuccess } from 'ts-decoders';

export function toNgValidator(decoder: Decoder<unknown>): ValidatorFn {
  return (control) => {
    const result = decoder.decode(control.value);

    if (isDecoderSuccess(result)) return null;

    return {
      decoderErrors: result.map((r) => r.message).join('\n'),
    };
  };
}
